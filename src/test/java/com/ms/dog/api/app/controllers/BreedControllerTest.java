package com.ms.dog.api.app.controllers;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.dog.api.app.entities.DescriptionBreed;
import com.ms.dog.api.app.entities.ImageBreed;
import com.ms.dog.api.app.entities.ImagesBreed;
import com.ms.dog.api.app.usecase.ports.input.BreedServiceImpl;

@SpringBootTest(properties = {})
@RunWith(SpringRunner.class)
public class BreedControllerTest {

	@InjectMocks
	private BreedController breedController;

	@Mock
	private static RestTemplate restTemplate;

	@MockBean
	private BreedServiceImpl breedServiceImp;

	@Value("${url.api.list.all}")
	private String urlAllBreeds;

	@Value("${url.api.list.one}")
	private String urlOneBreed;

	private DescriptionBreed descriptionBreed;
	private Exception exception;
	private static final String RS_DESC_BREEDS = "src/test/resources/RS-DESC-BREEDS.json";

	@BeforeEach
	public void setUp() throws JsonParseException, JsonMappingException, FileNotFoundException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		MockitoAnnotations.initMocks(this);
		breedController = new BreedController("https://dog.ceo/api/breeds/list/all",
				"https://dog.ceo/api/breed/{breed name}/images");
		descriptionBreed = mapper.readValue(new FileInputStream(RS_DESC_BREEDS), new TypeReference<DescriptionBreed>() {
		});
	}

	@AfterEach
	public void cleanUp() {
		exception = null;
		breedController = null;
	}

	@Test
	public void getBreedForNameTest() throws Exception {
		DescriptionBreed descriptionBreedOther = new DescriptionBreed();
		breedController = new BreedController("https://dog.ceo/api/breeds/list/all",
				"https://dog.ceo/api/breed/{breed name}/images");
		// # name
		descriptionBreedOther.setBreed("bulldog");
		// # subBreeds
		List<String> subBreeds = new ArrayList<String>();
		subBreeds.add("boston");
		subBreeds.add("english");
		subBreeds.add("french");
		descriptionBreedOther.setSubBreeds(subBreeds);
		// # Image of breeds
		ImagesBreed images = new ImagesBreed();
		List<ImageBreed> message = new ArrayList<ImageBreed>();
		ImageBreed imageBreed = new ImageBreed();
		imageBreed.setImagen("https://images.dog.ceo/breeds/bulldog-boston/n02096585_11776.jpg");
		message.add(imageBreed);
		images.setMessage(message);
		images.setStatus("ok");
		descriptionBreedOther.setImages(images);
		given(breedServiceImp.getBreedDescription("bulldog", urlAllBreeds, urlOneBreed))
				.willReturn(descriptionBreedOther);
		assertNotNull("No debe estar Null el RS ",
				ResponseEntity.ok(breedServiceImp.getBreedDescription("bulldog", this.urlAllBreeds, this.urlOneBreed)));
	}

	@Test
	public void getBreedForNameExceptionUrlAllTest() {
		breedController = new BreedController(null, "https://dog.ceo/api/breed/{breed name}/images");
		try {
			when(breedController.getBreedForName("bulldog")).thenThrow(new Exception("Internal communication error"));
		} catch (final Exception e) {
			exception = e;
		}
		assertAll(() -> assertNotNull("No debe estar Null la Exception ", exception),
				() -> assertEquals("Internal communication error", exception.getMessage()));
	}

	@Test
	public void getBreedForNameExceptionUrlOneTest() {
		breedController = new BreedController("https://dog.ceo/api/breeds/list/all", null);
		// Exception exception = null;
		try {
			when(breedController.getBreedForName("bulldog")).thenThrow(new Exception("Internal communication error"));
		} catch (final Exception e) {
			exception = e;
		}
		assertAll(() -> assertNotNull("No debe estar Null la Exception ", exception),
				() -> assertEquals("Internal communication error", exception.getMessage()));
	}

	@Test
	public void getBreedForNameExceptionUrlNullTest() {
		breedController = new BreedController(null, null);
		try {
			when(breedController.getBreedForName("bulldog")).thenThrow(new Exception("Internal communication error"));
		} catch (final Exception e) {
			exception = e;
		}
		assertAll(() -> assertNotNull("No debe estar Null la Exception ", exception),
				() -> assertEquals("Internal communication error", exception.getMessage()));
	}

}
