package com.ms.dog.api.app.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Breeds  implements Serializable  {
	private static final long serialVersionUID = -104897234115813729L;
	@Getter @Setter String status;
	@Getter @Setter HashMap<String,List<String>> message;
	@Getter @Setter int code;
}
