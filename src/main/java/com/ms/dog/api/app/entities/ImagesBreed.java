package com.ms.dog.api.app.entities;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImagesBreed implements Serializable {
	private static final long serialVersionUID = -2638814904564255292L;
	@Getter @Setter	List<ImageBreed> message;
	@Getter @Setter String status;
}
