package com.ms.dog.api.app;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
 

@SpringBootApplication
public class MsClientDogApiApplication {
	
    private static Logger log = LoggerFactory.getLogger(MsClientDogApiApplication.class);
    @lombok.Generated
	public static void main(String[] args) {
		SpringApplication.run(MsClientDogApiApplication.class, args);
		log.info("run run client");
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

}
