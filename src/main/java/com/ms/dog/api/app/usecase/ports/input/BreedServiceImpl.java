package com.ms.dog.api.app.usecase.ports.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.ms.dog.api.app.entities.Breeds;
import com.ms.dog.api.app.entities.DescriptionBreed;
import com.ms.dog.api.app.entities.ImageBreed;
import com.ms.dog.api.app.entities.ImagesBreed;

@Service
public class BreedServiceImpl implements BreedsService {

	private static Logger log = LoggerFactory.getLogger(BreedServiceImpl.class);
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public Breeds getAllRaces(String urlAllBreeds) {
		Breeds breeds = new Breeds();
		breeds.setMessage(new HashMap<String, List<String>>());
		breeds.setStatus("Not Found list");
		breeds.setCode(404);
		try {
			ResponseEntity<Breeds> response = restTemplate.exchange(urlAllBreeds, HttpMethod.GET, null,
					new ParameterizedTypeReference<Breeds>() {
					});
			breeds = Optional.ofNullable(response.getBody()).orElse(breeds);
		} catch (HttpClientErrorException e) {
			log.error(e.getMessage());
		}
		return breeds;
	}

	@Override
	public ImagesBreed getImagesBreed(String breed, String urlOneBreed) {
		ImagesBreed imagesBreed = new ImagesBreed();
		imagesBreed.setMessage(new ArrayList<ImageBreed>());
		imagesBreed.setStatus("Not Found list");
		try {
			ResponseEntity<ImagesBreed> response = restTemplate.exchange(remplaceBreedUtil(urlOneBreed, breed),
					HttpMethod.GET, null, new ParameterizedTypeReference<ImagesBreed>() {
					});
			imagesBreed = Optional.ofNullable(response.getBody()).orElse(imagesBreed);
		} catch (HttpClientErrorException e) {
			log.error(e.getMessage());
		}
		return imagesBreed;
	}

	@Override
	public DescriptionBreed getBreedDescription(String breed, String urlAllBreeds, String urlOneBreed) {
		DescriptionBreed descriptionBreed = new DescriptionBreed();
		descriptionBreed.setBreed(breed);
		descriptionBreed.setSubBreeds(Optional.of(this.getAllRaces(urlAllBreeds).getMessage()).get().get(breed));
		descriptionBreed.setImages(this.getImagesBreed(breed, urlOneBreed));
		return descriptionBreed;
	}

	public static String remplaceBreedUtil(String url, String breedSearh) {
		return url.replace("{breed name}", breedSearh);
	}

}
