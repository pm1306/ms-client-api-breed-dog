package com.ms.dog.api.app.usecase.ports.input;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.dog.api.app.entities.Breeds;
import com.ms.dog.api.app.entities.ImagesBreed;

@SpringBootTest
public class BreedServiceImplTest {

	@InjectMocks
	private static BreedServiceImpl breedServiceImpl;

	@Mock
	private static RestTemplate restTemplate;

	@Value("url.api.list.all")
	private String urlone;

	@Value("url.api.list.one")
	private String urlall;

	private static Breeds breeds;
	private static ImagesBreed imagesBreed;
	private static Breeds breedsNull;

	private static String urlAll;
	private static String url;

	private static final String RS_ALL_BREEDS = "src/test/resources/RS-ALL-BREEDS.json";
	private static final String RS_ONE_BREED = "src/test/resources/RS-ONE-BREED.json";
	private static final String RS_ALL_BREEDS_NULL = "src/test/resources/RS-ALL-BREEDS-NULL.json";

	@BeforeEach
	public void setUp() throws JsonParseException, JsonMappingException, FileNotFoundException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		MockitoAnnotations.initMocks(this);
		breeds = mapper.readValue(new FileInputStream(RS_ALL_BREEDS), new TypeReference<Breeds>() {
		});
		imagesBreed = mapper.readValue(new FileInputStream(RS_ONE_BREED), new TypeReference<ImagesBreed>() {
		});
		breedsNull = mapper.readValue(new FileInputStream(RS_ALL_BREEDS_NULL), new TypeReference<Breeds>() {
		});
		urlAll = "https://dog.ceo/api/breeds/list/all";
		url = "https://dog.ceo/api/breed/{breed name}/images";
	}

	@AfterAll
	public static void cleanUp() {
		breeds = null;
		imagesBreed = null;
		breedsNull = null;
		urlAll = null;
		url = null;
		Mockito.reset(restTemplate);
	}

	@Test
	@DisplayName("Genera el llamado inicial a la API")
	public void getBreedDescriptionTest() {
		ResponseEntity<Breeds> response = new ResponseEntity<Breeds>(breeds, HttpStatus.ACCEPTED);
		Mockito.when(restTemplate.exchange(urlAll, HttpMethod.GET, null, new ParameterizedTypeReference<Breeds>() {
		})).thenReturn(response);

		Mockito.when(restTemplate.exchange(remplaceBreedUtil(url, "bulldog"), HttpMethod.GET, null,
				new ParameterizedTypeReference<ImagesBreed>() {
				})).thenReturn(getImagesBreed());
		assertNotNull(breedServiceImpl.getBreedDescription("bulldog", urlAll, url),"No debe de venir vacia la respuesta");
		
	}

	public String remplaceBreedUtil(String url, String breedSearh) {
		return url.replace("{breed name}", breedSearh);
	}

	public ResponseEntity<ImagesBreed> getImagesBreed() {
		ResponseEntity<ImagesBreed> response = new ResponseEntity<ImagesBreed>(imagesBreed, HttpStatus.ACCEPTED);
		return response;
	}

	@Test
	@DisplayName("Valida llamado sea null con code 404")
	public void getAllRacesNullTest() {
		ResponseEntity<Breeds> response = new ResponseEntity<Breeds>(breedsNull, HttpStatus.ACCEPTED);
		Mockito.when(restTemplate.exchange("https://dog.ceo/api/breeds/list/all", HttpMethod.GET, null,
				new ParameterizedTypeReference<Breeds>() {
				})).thenReturn(response);
		assertAll(() -> assertNull(breedServiceImpl.getAllRaces(urlAll).getMessage()),
				() -> assertEquals("Not Found list", breedServiceImpl.getAllRaces(urlAll).getStatus(),
						"Se espera que el status se igual a \"Not Found list\""),
				() -> assertEquals(404, breedServiceImpl.getAllRaces(urlAll).getCode()));
	}
}
