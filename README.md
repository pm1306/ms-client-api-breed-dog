# Microservicio de cliente

_MS obtiene las url expuestas por el ms de configuracion y apartir de alli iniciar por el puerto
obtenido y logra comunicarse con otras api_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.

### Pre-requisitos 📋

_necesitas tener instalado **JAVA 8**   y  **Maven  3.6**_

```
java  =  https://openjdk.java.net/install/
maven =  https://maven.apache.org/ref/3.6.3/
```

### Instalación 🔧

_Dar doble click sobre el archivo install.cmd_

_**Si no**, lo puedes hacer pasos por paso  dentro del directorio **ms-client-dog-api** como detallo  a continuacion,  dentro de la consola msdos o la terminal de linux_

```
mvn clean  &&  mvn package
```

ejecutar

```
java  -jar  target/*.jar
```

Probar la api

iniciar el explorador de su equipo Chrome o Firefox  y con la url siguiente.

```
http://localhost:8013/api/search-breed?breed=bulldog
```

En caso de que actualice las url en el ms de configuracion puedes refrescar el ms cliente sin necesidad de bajar el ms, lo puedes hacer con la siguiente url pero la solicitud debe ser **POST**

```
http://localhost:8013/actuator/refresh
```

para mas detalles puedes verlo con

```
 http://localhost:8013/actuator
```
