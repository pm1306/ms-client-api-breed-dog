package com.ms.dog.api.app.usecase.ports.input;

import com.ms.dog.api.app.entities.Breeds;
import com.ms.dog.api.app.entities.DescriptionBreed;
import com.ms.dog.api.app.entities.ImagesBreed;

public interface BreedsService {
	public Breeds getAllRaces(String urlAllBreeds);
	public DescriptionBreed getBreedDescription(String breed, String urlAllBreeds, String urlOneBreed);
	public ImagesBreed getImagesBreed(String breed, String urlOneBreed);
}
