package com.ms.dog.api.app.entities;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class DescriptionBreed  implements Serializable {
	private static final long serialVersionUID = -7693261096019735967L;
	@Getter @Setter private String breed;
	@Getter @Setter private List<String> subBreeds;
	@Getter @Setter private ImagesBreed images;
}
