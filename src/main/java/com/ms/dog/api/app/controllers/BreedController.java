package com.ms.dog.api.app.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ms.dog.api.app.entities.DescriptionBreed;
import com.ms.dog.api.app.usecase.ports.input.BreedServiceImpl;

@RefreshScope
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({ "/api" })
public class BreedController {

	private static Logger log = LoggerFactory.getLogger(BreedController.class);

	@Autowired
	BreedServiceImpl breedServiceImp;

	private String urlAllBreeds;

	private String urlOneBreed;

	public BreedController(@Value("${url.api.list.all}") String urlAllBreeds,
			@Value("${url.api.list.one}") String urlOneBreed) {
		this.urlAllBreeds = urlAllBreeds;
		this.urlOneBreed = urlOneBreed;
	}

	@GetMapping("/search-breed")
	public ResponseEntity<DescriptionBreed> getBreedForName(@RequestParam(name = "breed", required = true) String raza)
			throws Exception {
		if (null != this.urlAllBreeds && null != this.urlOneBreed) {
			return ResponseEntity.ok(breedServiceImp.getBreedDescription(raza, this.urlAllBreeds, this.urlOneBreed));
		}
		log.error("The configuration server urls are not arriving.");
		throw new Exception("Internal communication error");

	}
}
